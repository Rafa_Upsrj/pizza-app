<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Locality;

class Address extends Model
{
    use HasFactory;

    protected $fillable = [
        'street',
        'location_id',
        'postal_code'
    ];

    public function locality()
    {
        return $this->belongsTo(Locality::class);
    }
}
