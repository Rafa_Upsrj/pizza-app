<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Locality;
class City extends Model
{
    use HasFactory;

    public function localities()
    {
        return $this->hasMany(Locality::class );
    }
}
