<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Address;
use App\Models\City;
use Illuminate\Database\Eloquent\Collection;

class Locality extends Model
{
    use HasFactory;

    public function addresses()
    {
        return $this->hasMany(Address::class );
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
