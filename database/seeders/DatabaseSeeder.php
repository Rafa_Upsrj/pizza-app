<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\User;
use App\Models\City;
use App\Models\Locality;
use App\Models\Address;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        User::factory(5)->create();
        City::factory(5)->create();
        Locality::factory(10)->create();
        Address::factory(20)->create();
    }
}
