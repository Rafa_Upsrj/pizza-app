<?php

use App\Http\Controllers\Api\v1\AddressController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/v1/address', [AddressController::class, 'index']);
Route::post('/v1/address', [AddressController::class, 'store']);
Route::get('/v1/address/{address}', [AddressController::class, 'show']);