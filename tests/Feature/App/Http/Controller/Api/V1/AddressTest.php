<?php

namespace Tests\Feature\App\Http\Controller\Api\V1;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class AddressTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_all_address()
    {
        $response = $this->get('api/v1/address');

        $response->assertStatus(200)
                ->assertJson(fn (AssertableJson $json) =>
                    $json->has(0, fn ($json) =>
                        $json->hasAll('id','street','location_id')
                            ->etc()
                        )
                );
    }
    public function test_get_address()
    {
        $response = $this->get('api/v1/address/1');

        $response->assertStatus(200)
                ->assertJson(fn (AssertableJson $json) =>
                        $json->hasAll('id','street','location_id')
                            ->etc()
                        
                );
    }
    public function test_store_address()
    {
        $address = [
            'street' => 'calle 2',
            'location_id' => 1,
            'postal_code' => 555
        ];
        $response = $this->post('api/v1/address/',$address);
        $response->assertStatus(201)
                ->assertJson(fn (AssertableJson $json) =>
                        $json->hasAll('id','street','location_id')
                            ->etc()
                        
                );
        $address = $response->getData();
        $this->assertDatabaseHas('addresses', [
            'street' => $address->street,
            'id' => $address->id,
        ]);
    }
}
